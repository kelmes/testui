/* window.vala
 *
 * Copyright 2018 Kieran Elmes
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Testui {
	[GtkTemplate (ui = "/org/gnome/Testui/window.ui")]
	
	public class Window : Gtk.ApplicationWindow {
	    private bool use_custom;
	    private int num_pages = 5;

        [GtkChild]
        private Gtk.HeaderBar header_bar;
        private Gtk.Button open_button;
        private Gtk.Box pages_box;
		public Window (Gtk.Application app) {
			Object (application: app);
			this.use_custom = true;
			
			
			header_bar.set_title("title title title");
			header_bar.set_show_close_button(true);
			
			// Gtk.Button test_button;
			// test_button = new Gtk.Button(); 
			pages_box.pack_end(new Gtk.Label("1"), false, false, 0);
		}
	}
}
